/* Copyright 2013-present Barefoot Networks, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "include/headers_tcp.p4"
#include "include/parser_tcp.p4"
#include "include/intrinsic.p4"
#include "include/queueing.p4"
#define ENTRIES 4

//////////////////// metadata needed for stats ///////////////////
header_type stats_metadata_t {
    fields {
	meter_tag : 32;
    mark_count : 32;
    drop_count : 32;
    }
}
metadata stats_metadata_t stats_metadata;

//////////////////// routing tables and action ///////////////////
action _drop() {
    drop();
}

header_type routing_metadata_t {
    fields {
        nhop_ipv4 : 32;
    }
}

metadata routing_metadata_t routing_metadata;

action set_nhop(nhop_ipv4, port) {
    modify_field(routing_metadata.nhop_ipv4, nhop_ipv4);
    modify_field(standard_metadata.egress_spec, port);
    add_to_field(ipv4.ttl, -1);
}

table ipv4_lpm {
    reads {
        ipv4.dstAddr : lpm;
    }
    actions {
        set_nhop;
        _drop;
    }
    size: 8;
}

action set_dmac(dmac) {
    modify_field(ethernet.dstAddr, dmac);
}
table forward {
    reads {
        routing_metadata.nhop_ipv4 : exact;
    }
    actions {
        set_dmac;
        _drop;
    }
    size: 8;
}

action rewrite_mac(smac) {
    modify_field(ethernet.srcAddr, smac);
}
table send_frame {
    reads {
        standard_metadata.egress_port: exact;
    }
    actions {
        rewrite_mac;
        _drop;
    }
    size: 8;
}

//////////////////// meter ///////////////////
/*
meter my_meter {
    type: bytes; // or bytes
    static: m_table;
    //instance_count: 16384;
    instance_count: 512;
}

action m_action(meter_idx) {
    execute_meter(my_meter, meter_idx, stats_metadata.meter_tag);
    //modify_field(standard_metadata.egress_spec, 1);
}
*/
meter a_meter {
    type: bytes;
    static: m_table;
    instance_count: 1;
}

register qos_pkt_count {
    width: 32;
    instance_count : 4;
}

action a_m_action() {
    execute_meter(a_meter, 0, stats_metadata.meter_tag);
}

table m_table {
    reads {
        ethernet.srcAddr : exact;
    }
    actions {
        a_m_action;
        _nop;
    }
    size : 8;
}

action m_drop() {
    drop();
    register_read(stats_metadata.drop_count, qos_pkt_count, 0);
    add_to_field(stats_metadata.drop_count, 1);
    register_write(qos_pkt_count, 0, stats_metadata.drop_count);
}

action marking(){
    modify_field(ipv4.ecn, 3);
    register_read(stats_metadata.mark_count, qos_pkt_count, 1);
    add_to_field(stats_metadata.mark_count, 1);
    register_write(qos_pkt_count, 1, stats_metadata.mark_count);
    
}

action _nop() {
    no_op();
}

table m_filter {
    reads {
        stats_metadata.meter_tag : exact;
    }
    actions {
        _drop;
        m_drop;
        marking;
        _nop;
    }
    size: 8;
}

/////////////////// queueing ///////////////////////////
register ingress_global_timestamp {
    width: 48;
    instance_count : ENTRIES;
}

register enq_timestamp {
    width: 48;
    instance_count : ENTRIES;
}

register enq_qdepth {
    width: 16;
    instance_count : ENTRIES;
}

register deq_timedelta {
    width: 32;
    instance_count : ENTRIES;
}

register deq_qdepth {
    width: 16;
    instance_count : ENTRIES;
}

action set_queue_register() {
    register_write(enq_qdepth, 0, queueing_metadata.enq_qdepth);
}

table set_queue_info {
    actions {
        set_queue_register;
    }
}

/////////////////// control  ///////////////////////
control ingress {	
    apply(ipv4_lpm);
    apply(forward);
   
    apply(m_table);
    apply(m_filter);  
}

control egress {    
    apply(set_queue_info);
    apply(send_frame);
}